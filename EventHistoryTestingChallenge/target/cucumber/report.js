$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("add-event.feature");
formatter.feature({
  "line": 1,
  "name": "When an Event is created, it should appear in the table view.",
  "description": "",
  "id": "when-an-event-is-created,-it-should-appear-in-the-table-view.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 27089895171,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Validate Add Event",
  "description": "",
  "id": "when-an-event-is-created,-it-should-appear-in-the-table-view.;validate-add-event",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enter \u0027Add Test\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "event \u0027Add Test\u0027 is added",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 6798927397,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 4180152956,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add Test",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 9711940616,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 2633319471,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 3297919243,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Add Test",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 352308919,
  "status": "passed"
});
formatter.after({
  "duration": 694314957,
  "status": "passed"
});
formatter.uri("event-add-order.feature");
formatter.feature({
  "line": 1,
  "name": "Events should be sorted in descending chronological order by date",
  "description": "(with the most recent event on top of the list).",
  "id": "events-should-be-sorted-in-descending-chronological-order-by-date",
  "keyword": "Feature"
});
formatter.before({
  "duration": 15638296515,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "Validate Add Event order",
  "description": "",
  "id": "events-should-be-sorted-in-descending-chronological-order-by-date;validate-add-event-order",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user enter \u0027Test 1\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "event \u0027Test 1\u0027 is added",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "user enter \u0027Test 2\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "event \u0027Test 2\u0027 is added",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "event name \u0027Test 1\u0027 is at \u00275\u0027",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "event name \u0027Test 2\u0027 is at \u00276\u0027",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 1861249923,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2227120096,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 1",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 7513487200,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 3680888973,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 2507247750,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 1",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 349557233,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 1947504530,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2283379840,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 2",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 9753468975,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 4503368670,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 3799881947,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 2",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 365096103,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 1",
      "offset": 12
    },
    {
      "val": "5",
      "offset": 27
    }
  ],
  "location": "commonSteps.verify_event_order(String,int)"
});
formatter.result({
  "duration": 1682897195,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test 2",
      "offset": 12
    },
    {
      "val": "6",
      "offset": 27
    }
  ],
  "location": "commonSteps.verify_event_order(String,int)"
});
formatter.result({
  "duration": 1691335453,
  "status": "passed"
});
formatter.after({
  "duration": 701018116,
  "status": "passed"
});
formatter.uri("event-date-format.feature");
formatter.feature({
  "line": 1,
  "name": "In the table view, when we show the date for an Event, the format is “yyyy-MM-dd HH:mm”.",
  "description": "This is fine for most events, but if the Event was created today or yesterday,\nthen it should display “Today HH:mm” or “Yesterday HH:mm” instead",
  "id": "in-the-table-view,-when-we-show-the-date-for-an-event,-the-format-is-“yyyy-mm-dd-hh:mm”.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 16814604991,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Validate Event date format",
  "description": "",
  "id": "in-the-table-view,-when-we-show-the-date-for-an-event,-the-format-is-“yyyy-mm-dd-hh:mm”.;validate-event-date-format",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user enter \u0027Test\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "event \u0027Test\u0027 is added",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "event \u00275\u0027 date show today",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 2009531923,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2258502220,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 7510362445,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 2676261781,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 3176442486,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Test",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 380951904,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "5",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_event_date(int)"
});
formatter.result({
  "duration": 1425825401,
  "error_message": "java.lang.AssertionError: expected [true] but found [false]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertTrue(Assert.java:44)\n\tat org.testng.Assert.assertTrue(Assert.java:54)\n\tat stepDefinitions.commonSteps.verify_event_date(commonSteps.java:79)\n\tat ✽.And event \u00275\u0027 date show today(event-date-format.feature:13)\n",
  "status": "failed"
});
formatter.after({
  "duration": 283341533,
  "status": "passed"
});
formatter.uri("event-name-required.feature");
formatter.feature({
  "line": 1,
  "name": "he Event name property is required. An Event should not be allowed to be created (or updated) with an empty name.",
  "description": "(If Save is clicked with an empty name, the user should remain on the Event screen)",
  "id": "he-event-name-property-is-required.-an-event-should-not-be-allowed-to-be-created-(or-updated)-with-an-empty-name.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 13170260856,
  "status": "passed"
});
formatter.scenario({
  "line": 6,
  "name": "Validate Event Name is required Positive test case",
  "description": "",
  "id": "he-event-name-property-is-required.-an-event-should-not-be-allowed-to-be-created-(or-updated)-with-an-empty-name.;validate-event-name-is-required-positive-test-case",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "user navigate to add event page",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 1963509552,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2111513406,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 1486089627,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 60476393481,
  "error_message": "org.openqa.selenium.NoSuchElementException: An element could not be located on the page using the given search parameters.\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027SAIs-Air\u0027, ip: \u00272600:8801:9200:f3:ac67:7311:81db:22d8\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.14\u0027, java.version: \u00271.8.0_121\u0027\nDriver info: io.appium.java_client.ios.IOSDriver\nCapabilities {app: /Users/saigopalakrishnamada..., automationName: XCUITest, browserName: , databaseEnabled: false, deviceName: iPhone 8 Plus, javascriptEnabled: true, locationContextEnabled: false, networkConnectionEnabled: false, platform: MAC, platformName: iOS, takesScreenshot: true, udid: 8AA3992D-3177-491A-B386-E0E..., webStorageEnabled: false}\nSession ID: aa729d7f-4821-4306-9bd0-c2bea99cde1f\n*** Element info: {Using\u003dxpath, value\u003d//XCUIElementTypeStaticText[@name\u003d\u0027editEventName\u0027]}\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.execute(AppiumCommandExecutor.java:231)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:42)\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\n\tat io.appium.java_client.ios.IOSDriver.execute(IOSDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:322)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:62)\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\n\tat io.appium.java_client.ios.IOSDriver.findElement(IOSDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:424)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElementByXPath(DefaultGenericMobileDriver.java:152)\n\tat io.appium.java_client.AppiumDriver.findElementByXPath(AppiumDriver.java:1)\n\tat io.appium.java_client.ios.IOSDriver.findElementByXPath(IOSDriver.java:1)\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:314)\n\tat io.appium.java_client.DefaultGenericMobileDriver.findElement(DefaultGenericMobileDriver.java:58)\n\tat io.appium.java_client.AppiumDriver.findElement(AppiumDriver.java:1)\n\tat io.appium.java_client.ios.IOSDriver.findElement(IOSDriver.java:1)\n\tat stepDefinitions.commonSteps.navigate_add_event(commonSteps.java:33)\n\tat ✽.And user navigate to add event page(event-name-required.feature:10)\n",
  "status": "failed"
});
formatter.after({
  "duration": 346503353,
  "status": "passed"
});
formatter.before({
  "duration": 12983374556,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Validate Event Name is required negative test case",
  "description": "",
  "id": "he-event-name-property-is-required.-an-event-should-not-be-allowed-to-be-created-(or-updated)-with-an-empty-name.;validate-event-name-is-required-negative-test-case",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 12,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 14,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 15,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "user should not navigates to events page",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 1988645065,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2348006970,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 1686323476,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.not_navigate_event()"
});
formatter.result({
  "duration": 2548191154,
  "error_message": "java.lang.AssertionError: User not able to navigate to events page expected [false] but found [true]\n\tat org.testng.Assert.fail(Assert.java:96)\n\tat org.testng.Assert.failNotEquals(Assert.java:776)\n\tat org.testng.Assert.assertFalse(Assert.java:65)\n\tat stepDefinitions.commonSteps.not_navigate_event(commonSteps.java:72)\n\tat ✽.And user should not navigates to events page(event-name-required.feature:17)\n",
  "status": "failed"
});
formatter.after({
  "duration": 485028698,
  "status": "passed"
});
formatter.uri("update-event.feature");
formatter.feature({
  "line": 1,
  "name": "When an Event is updated, the changes should appear in the table view.",
  "description": "",
  "id": "when-an-event-is-updated,-the-changes-should-appear-in-the-table-view.",
  "keyword": "Feature"
});
formatter.before({
  "duration": 10958032589,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Validate Update Event",
  "description": "",
  "id": "when-an-event-is-updated,-the-changes-should-appear-in-the-table-view.;validate-update-event",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Appium"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "user clicks on Add Event",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "user enter \u0027Update Test\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "event \u0027Update Test\u0027 is added",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "user clicks on \u0027Update Test\u0027 event",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "user navigate to add event page",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "user enter \u0027New Test\u0027 into name",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "user clicks save event",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "user navigates to events page",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "event \u0027New Test\u0027 is added",
  "keyword": "And "
});
formatter.match({
  "location": "commonSteps.click_add_event()"
});
formatter.result({
  "duration": 2216212387,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2332612790,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Update Test",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 8826880691,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 2871482069,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 3368461826,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Update Test",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 387895720,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Update Test",
      "offset": 16
    }
  ],
  "location": "commonSteps.click_event(String)"
});
formatter.result({
  "duration": 903925370,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_add_event()"
});
formatter.result({
  "duration": 2298054603,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Test",
      "offset": 12
    }
  ],
  "location": "commonSteps.i_validate_Custom_View(String)"
});
formatter.result({
  "duration": 9275794862,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.save_event()"
});
formatter.result({
  "duration": 2742822036,
  "status": "passed"
});
formatter.match({
  "location": "commonSteps.navigate_event()"
});
formatter.result({
  "duration": 3349701319,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "New Test",
      "offset": 7
    }
  ],
  "location": "commonSteps.verify_added_event(String)"
});
formatter.result({
  "duration": 596187021,
  "status": "passed"
});
formatter.after({
  "duration": 654625089,
  "status": "passed"
});
});