package runner;

import cucumber.api.CucumberOptions;
import org.testng.annotations.Test;
import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(features = {"src//test//java//features"}
   ,glue = {"stepDefinitions","utility"}, plugin = {"pretty", "html:target/cucumber"}, tags = {"@Appium"})

@Test
public class RunTest extends AbstractTestNGCucumberTests {
}
