package utility;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.IOSMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Hook {

    private static WebDriver driver;
    private static AppiumDriver<MobileElement> appium;

    @Before("@web")
    public void setUp() {
        System.setProperty("Webdriver.chrome.driver",System.getProperty("user.dir")+"//drivers//chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Before("@Appium")
    public void setUpAppium() throws Throwable {
        System.out.println("appium running");
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "XCUITest");
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "IOS");
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 8 Plus");
        cap.setCapability(MobileCapabilityType.UDID,"8AA3992D-3177-491A-B386-E0E7CC33075C");
        cap.setCapability(MobileCapabilityType.APP, System.getProperty("user.dir")+"//App//EventHistoryChallenge.app");
        appium = new IOSDriver<>(new URL("http://0.0.0.0:4723/wd/hub"), cap);
        appium.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        appium.quit();
    }

    public static WebDriver getDriver() {
        return appium;
    }
}
