Feature: When an Event is created, it should appear in the table view.

  @Appium
  Scenario: Validate Add Event
    Given user clicks on Add Event
    When user navigate to add event page
    Then user enter 'Add Test' into name
    And user clicks save event
    And user navigates to events page
    And event 'Add Test' is added




