Feature: he Event name property is required. An Event should not be allowed to be created (or updated) with an empty name.
(If Save is clicked with an empty name, the user should remain on the Event screen)


@Appium
Scenario: Validate Event Name is required Positive test case
Given user clicks on Add Event
When user navigate to add event page
And user clicks save event
And user navigate to add event page

@Appium
Scenario: Validate Event Name is required negative test case
Given user clicks on Add Event
When user navigate to add event page
And user clicks save event
And user should not navigates to events page