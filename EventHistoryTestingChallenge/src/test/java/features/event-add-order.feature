Feature: Events should be sorted in descending chronological order by date
  (with the most recent event on top of the list).

  @Appium
  Scenario: Validate Add Event order
    Given user clicks on Add Event
    When user navigate to add event page
    Then user enter 'Test 1' into name
    And user clicks save event
    And user navigates to events page
    And event 'Test 1' is added
    Given user clicks on Add Event
    When user navigate to add event page
    Then user enter 'Test 2' into name
    And user clicks save event
    And user navigates to events page
    And event 'Test 2' is added
    And event name 'Test 1' is at '5'
    And event name 'Test 2' is at '6'