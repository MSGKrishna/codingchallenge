Feature: In the table view, when we show the date for an Event, the format is “yyyy-MM-dd HH:mm”.
  This is fine for most events, but if the Event was created today or yesterday,
  then it should display “Today HH:mm” or “Yesterday HH:mm” instead

  @Appium
  Scenario: Validate Event date format
    Given user clicks on Add Event
    When user navigate to add event page
    Then user enter 'Test' into name
    And user clicks save event
    And user navigates to events page
    And event 'Test' is added
    And event '5' date show today