Feature: When an Event is updated, the changes should appear in the table view.

  @Appium
  Scenario: Validate Update Event
  Given user clicks on Add Event
  When user navigate to add event page
  Then user enter 'Update Test' into name
  And user clicks save event
  And user navigates to events page
  And event 'Update Test' is added
  And user clicks on 'Update Test' event
  When user navigate to add event page
  Then user enter 'New Test' into name
  And user clicks save event
  And user navigates to events page
  And event 'New Test' is added