package stepDefinitions;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import io.appium.java_client.MobileElement;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import utility.Hook;


public class commonSteps {

    private WebDriver driver;

    public commonSteps() {
        this.driver = Hook.getDriver();
    }

    @Given("^user clicks on Add Event$")
    public void click_add_event() throws Throwable {
        System.out.println("clicks on Add Event");
        driver.findElement(By.xpath("//*[@name='Add Event']")).click();
    }

    @When("^user navigate to add event page$")
    public void navigate_add_event() throws Throwable {
        System.out.println("user navigate to add event page");
        Assert.assertTrue(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='editEventName']")).isDisplayed(),"User not able to navigate to add event page");
    }

    @Then("^user enter '(.*)' into name$")
    public void i_validate_Custom_View(String name) throws Throwable {
        System.out.println("user clicks name");
        driver.findElement(By.xpath("//XCUIElementTypeTextField[@name='editEventNameField']")).click();
        System.out.println("user enter name");
        driver.findElement(By.xpath("//XCUIElementTypeTextField[@name='editEventNameField']")).clear();
        driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='editEventName']")).sendKeys(name);
    }

    @When("^user clicks save event$")
    public void save_event() throws Throwable {
        System.out.println("user save event");
        driver.findElement(By.xpath("//XCUIElementTypeButton[@name='Save']")).click();
    }

    @Then("^user navigates to events page$")
    public void navigate_event() throws Throwable {
        System.out.println("user navigate to events page");
        Assert.assertTrue(driver.findElement(By.xpath("//*[@name='Add Event']")).isEnabled(),"User not able to navigate to events page");
    }

    @Then("^event '(.*)' is added$")
    public void verify_added_event(String event) throws Throwable {
        System.out.println("user navigate to add event");
        Assert.assertTrue(driver.findElement(MobileBy.ByAccessibilityId.id(event)).isEnabled(),"Event not added");
    }

    @Then("^user clicks on '(.*)' event$")
    public void click_event(String event) throws Throwable {
        System.out.println("user clicks on added event");
        driver.findElement(MobileBy.ByAccessibilityId.id(event)).click();
    }

    @Then("^user should not navigates to events page$")
    public void not_navigate_event() throws Throwable {
        System.out.println("user should not navigate to events page");
        Assert.assertFalse(driver.findElement(By.xpath("//*[@name='Add Event']")).isEnabled(),"User not able to navigate to events page");
    }

    @Then("^event '(.*)' date show today$")
    public void verify_event_date(int event) throws Throwable {
        System.out.println("Event date display today");
        String date = driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='eventCell-"+event+"-Date']")).getText();
        Assert.assertTrue(date.contains("Today"));
    }

    @Then("^event name '(.*)' is at '(.*)'$")
    public void verify_event_order(String name, int order) throws Throwable {
        System.out.println("Event display today");
        Assert.assertTrue(driver.findElement(By.xpath("//XCUIElementTypeStaticText[@name='eventCell-"+order+"-Name' and @label='"+name+"']")).isDisplayed());

    }
}

//*[@name="eventCell-7-Name" and @label='f']